import Vue from 'vue'
import App from './Calendar.vue'

import moment from 'moment';
moment.locale('en_GB');

Vue.config.productionTip = false;

new Vue({
    render: h => h(App),
}).$mount('#app');
