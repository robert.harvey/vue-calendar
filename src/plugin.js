import Calendar from './Calendar.vue';

module.exports = {
    install: function (Vue) {
        Vue.component('vue-calendar', Calendar);
    }
};